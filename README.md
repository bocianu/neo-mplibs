# Neo6502 Mad-Pascal libraries

A set of custom libraries for MadPascal that cover the range of API functions for the Neo6502, and a couple of auxiliary tools to make programming on the Neo6502 easier.

Lastest documentation always at: https://bocianu.gitlab.io/neo-mplibs/

Neo6502 API documentation: https://github.com/paulscottrobson/neo6502-firmware/wiki

### INSTALLATION:

#### For dummies:
Just copy all (or needed) *.pas files into your project directory

#### For common users:
Copy all *.pas files into directory 'lib' in your MadPascal compiler folder and update possibly obsolete versions

#### For advanced users:
At the beginning of your program code, add following line:
```
{$librarypath path_to_neo_mplibs}
```

#### For expert users:
Create symbolic links of all library *.pas files, into 'lib' directory of your MadPascal complier folder.


